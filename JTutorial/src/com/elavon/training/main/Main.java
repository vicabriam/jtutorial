package com.elavon.training.main;

import com.elavon.training.impl.*;

public class Main {

    public static void main(final String[] args) {
        final ImplementAwesomeness ia = new ImplementAwesomeness();
        System.out.println("Sum: " + ia.getSum(5, 1));
        System.out.println("Difference: " + ia.getDifference(10, 5));
        System.out.println("Product : " + ia.getProduct(10, 5));
        System.out.println(ia.getQuotientAndRemainder(125, 25));
        System.out.println(ia.getQuotientAndRemainder(125, 20));
        System.out.println("Fahrenheit to Celsius: " + ia.toCelsius(100));
        System.out.println("Celsius to Fahrenheit: " + ia.toFahrenheit(0));
        System.out.println("Kilo to Pound:" + ia.toPound(51.5));
        System.out.println("Pound to Kilo: " + ia.toKilogram(100));
        System.out.println(ia.isPalindrome("level"));
        System.out.println(ia.isPalindrome("river"));
    }
}
