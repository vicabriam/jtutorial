package com.elavon.training.impl;

import com.elavon.training.interf.AwesomeCalculator;

public class ImplementAwesomeness implements AwesomeCalculator {

    @Override
    public int getSum(final int augend, final int addend) {
        return augend + addend;
    }

    @Override
    public double getDifference(final double minuend, final double subtrahend) {
        return minuend - subtrahend;
    }

    @Override
    public double getProduct(final double multiplicand, final double multiplier) {
        return multiplicand * multiplier;
    }

    @Override
    public String getQuotientAndRemainder(final int dividend, final int divisor) {
        final int quotient = dividend / divisor;
        final int remainder = dividend % divisor;
        return quotient + " remainder " + remainder;
    }

    @Override
    public double toCelsius(final int fahrenheit) {
        return (fahrenheit - 32) * 1.8;
    }

    @Override
    public double toFahrenheit(final int celsius) {
        return celsius * 1.8 + 32;
    }

    @Override
    public double toKilogram(final double lbs) {
        return lbs / 2.2;
    }

    @Override
    public double toPound(final double kg) {
        return kg * 2.2;
    }

    @Override
    public boolean isPalindrome(final String str) {
        int i;
        final int n = str.length();
        String str1 = "";

        for (i = n - 1; i >= 0; i--) {
            str1 = str1 + str.charAt(i);
        }

        if(str1.equals(str)) {
            return true;
        } else {
            return false;
        }
    }
}
