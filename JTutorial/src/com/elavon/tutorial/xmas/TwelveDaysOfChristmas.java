package com.elavon.tutorial.xmas;

public class TwelveDaysOfChristmas {
	public static void printLyrics() {
		System.out.println("TWELVE DAYS OF CHRISTMAS");
		String gift1 = "A partridge in a pear tree!\n";
		String gift2 = "Two turtle doves!\nAnd";
		String gift3 = "Three French hens!,";
		String gift4 = "Four calling birds!,";
		String gift5 = "Five golden rings!,";
		String gift6 = "Six geese-a-laying!,";
		String gift7 = "Seven swans-a-swimming!,";
		String gift8 = "Eight maids-a-milking!,";
		String gift9 = "Nine ladies dancing!,";
		String gift10 = "Ten lords-a-leaping!,";
		String gift11 = "Eleven pipers piping!,";
		String gift12 = "Twelve drummers drumming!,";
		for(int gifts = 1; gifts <= 12; gifts++) {
			String str = Integer.toString(gifts); 
			System.out.println("On the " + days(str) + " day of Christmas, my true love sent to me, ");
			
			switch(gifts) {
			case 12:
				System.out.println(gift12);
			case 11:
				System.out.println(gift11);
			case 10:
				System.out.println(gift10);
			case 9:
				System.out.println(gift9);
			case 8:
				System.out.println(gift8);
			case 7:
				System.out.println(gift7);
			case 6:
				System.out.println(gift6);
			case 5:
				System.out.println(gift5);
			case 4:
				System.out.println(gift4);
			case 3:
				System.out.println(gift3);
			case 2:
				System.out.println(gift2);
			case 1:
				System.out.println(gift1);
			}
		}
	}
	
	public static String days(String order){
		if(order.equals("1")) {
			return "first";
		}
		else if(order.equals("2")) {
			return "second";
		}
		else if(order.equals("3")) {
			return "third";
		}
		else if(order.equals("4")) {
			return "fourth";
		}
		else if(order.equals("5")) {
			return "fifth";
		}
		else if(order.equals("6")) {
			return "sixth";
		}
		else if(order.equals("7")) {
			return "seventh";
		}
		else if(order.equals("8")) {
			return "eighth";
		}
		else if(order.equals("9")) {
			return "ninth";
		}
		else if(order.equals("10")) {
			return "tenth";
		}
		else if(order.equals("11")) {
			return "eleventh";
		}
		else if(order.equals("12")) {
			return "twelfth";
		}
		else{
			return order;
		}
	}
	
	public static void main(String[] args) {
		TwelveDaysOfChristmas.printLyrics();
	}
}
