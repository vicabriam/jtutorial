package com.elavon.tv;

public class TV {
	protected String brand;
	protected String model;
	protected static boolean power;
	protected static int channel;
	protected static int volume;

	TV(String brandName, String modelName) {
		brand = brandName;
		model = modelName;
		power = false;
		channel = 0;
		volume = 5;
	}

	public void turnOn() {
		power = true;
	}

	public void turnOff() {
		power = false;
	}

	public void channelUp() {
		channel++;
		if(channel > 13){
			channel = 13;
		}
	}

	public void channelDown() {
		channel--;
		if(channel < 0){
			channel = 0;
		}
	}

	public void volumeUp() {
		volume++;
		if(volume > 10){
			volume = 10;
		}
	}

	public void volumeDown() {
		volume--;
		if(volume < 0){
			volume = 0;
		}
	}

	@Override
	public String toString() {
		return "BRANDNAME: " + brand + " " + "MODEL: " + model + "[" + "ON: "
				+ power + ", " + "CHANNEL: " + channel + ", " + "VOLUME: "
				+ volume + "]";
	}

	public static void main(String[] args) {
		TV t1 = new TV("Andre Electronics, ", "ONE");
		t1.turnOn();
		
		for(int i = 0; i <= 4; i++){
			t1.channelUp();
		}
		
		t1.channelDown();
		
		for(int j = 0; j <= 2; j++){
			t1.volumeDown();
		}
		
		t1.volumeUp();
		
		t1.turnOff();
		
		System.out.println(t1);
	}
}
