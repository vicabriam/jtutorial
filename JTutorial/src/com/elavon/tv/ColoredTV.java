package com.elavon.tv;

public class ColoredTV extends TV{
	protected int brightness;
	protected int contrast;
	protected int picture;
	
	ColoredTV(String brandName, String modelName) {
		super(brandName, modelName);
		brightness = 50;
		contrast = 50;
		picture = 50;
	}
	
	public void brightnessUp(){
		brightness ++;
	}
	
	public void brightnessDown(){
		brightness --;
	}
	
	public void contrastUp(){
		contrast ++;
	}
	
	public void contrastDown(){
		contrast --;
	}
	
	public void pictureUp(){
		picture ++;
	}
	
	public void pictureDown(){
		picture --;
	}
	
	public void switchToChannel(int channelX){
		channel = channelX;
	}
	
	public void mute(){
		volume = 0;
	}
	
	@Override
	public String toString() {
		return super.toString() + "[" + "BRIGHTNESS: " + brightness + ", " + "CONTRAST: " + contrast + ", " + "PICTURE: " + picture + "]";
	}
	
	public static void main(String[] args) {
//		TV bnwTV;
//		ColoredTV sonyTV;
		ColoredTV sharpTV;
//		bnwTV = new TV("Admiral", "A1");
//		sonyTV = new ColoredTV("SONY", "S1");
		sharpTV = new ColoredTV("SHARP", "SH1");
		
//		sharpTV.mute();
//		sonyTV.brightnessUp();
		
		sharpTV.switchToChannel(69);
		
		for(int v = 1; v <= 14; v++){
			sharpTV.brightnessUp();
		}
		
		for(int v = 1; v <= 4; v++){
			sharpTV.brightnessDown();
		}
		
		for(int v = 1; v <= 5; v++){
			sharpTV.contrastDown();
		}
		
		for(int v = 1; v <= 10; v++){
			sharpTV.contrastUp();
		}
		
		for(int v = 1; v <= 20; v++){
			sharpTV.pictureDown();
		}
		
		for(int v = 1; v <= 10; v++){
			sharpTV.pictureUp();
		}
		
//		System.out.println(bnwTV);
//		System.out.println(sonyTV);
		System.out.println(sharpTV);
	}

}
