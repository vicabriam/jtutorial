package com.elavon.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ArrayCollectionsExercise {

    public static void main(String[] args) {
        // 2
        String[] countries = { "Japan", "Singapore", "United Kingdom", "Italy", "Netherlands", "USA", "Korea", "Spain",
            "Greece", "New Zealand" };
        // 3
        System.out.println(countries.length);
        // 4
        List<String> count = new ArrayList<>(Arrays.asList(countries));
        // 5
        System.out.println(count.size());
        // 6
        count.add("Scotland");
        System.out.println(count);
        // 7
        if (countries.length == 0) {
            System.out.println("Array is empty.");
        }
        else {
            System.out.println("Array has contents.");
        }
        // 8
        System.out.println(count.size());
        // 9
        if (count.contains("Mongolia")) {
            System.out.println("Mongolia is in the array.");
        }
        else {
            System.out.println("Mongolia is not in the array.");
        }
        // 10
        if (count.contains("Scotland")) {
            System.out.println("Scotland is in the array.");
        }
        else {
            System.out.println("Scotland is not in the array.");
        }
        // 11
        count.remove(count.size()-1);
        System.out.println(count);
        // 12
        if (count.contains("Indonesia") && count.contains("United Kingdom") && count.contains("France")
                && count.contains("Italy") && count.contains("USA")) {
            System.out.println("All top countries made it to the list.");
        }
        else {
            System.out.println("Not all top countries made it to the list.");
        }
        // 13
        System.out.println(count);
        // 14
        Map<Integer, String> countryMap = new HashMap<>();
        int[] numbers = {1,2,3,4,5,6,7,8,9,10};
        
        for(int j=0; j < countries.length; j++){
            countryMap.put(numbers[j], countries[j]);
        }
        // 15
        System.out.println("List of countries by rank: " + countryMap);
        // 16
        Collections.sort(count);
        // 17
        System.out.println("List of countries alphabetically : " + count);
        // 18
        for(Entry<Integer, String> key : countryMap.entrySet()) {
            System.out.println("Rank #" + key.getKey() + ": " + key.getValue());
        }
    }
}
